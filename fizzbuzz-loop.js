// run this next sequence of code under these conditions
//the conditions being..start at i, do it this many times, and increment by this much)
for (var i = 1; i <= 20 ; i++)
{
  //store value of remainders
  var theFizz = i % 3;
  var theBuzz = i % 5;
  //print fizz under neccesary condition
  if((theFizz == 0) && (theBuzz != 0)){
  console.log('FIZZ')
  }
  //print buzz under neccesary condition
  else if((theBuzz == 0) && (theFizz != 0)){
  console.log('BUZZ')
  }
  //print fizzbuzz under neccesary condition
  else if((theBuzz == 0) && (theFizz == 0)){
  console.log('FIZZBUZZ')
  }
  //if no other conditions have been satisfied, print the value of i in the console
   else{
   console.log(i);
   }
}
