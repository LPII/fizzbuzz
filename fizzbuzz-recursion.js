//init function and pass the limit as an argument

function fizzBuzz(cap) {
//base case for cap
    if(cap === 0) {
    return 1;
 }
//standardizing the argument input in case it falls out of the neccesary case
   if(cap < 0) {
      return console.log("Only positive numbers!!");
   }
//conditional
   if((cap % 3 === 0) && (cap % 5 === 0)) {
        console.log("fizzbuzz");
   }
   else if (cap % 3 === 0) {
       console.log("fizz");
   }
   else if (cap % 5 === 0) {
       console.log("buzz");
   }
   else {
       console.log(cap);
  }
  //init recursion by calling itself...within itself...func-ception
  //also decrement the arguments value by one
    fizzBuzz(cap-1);
}
// function call with whatever argument
fizzBuzz(20);
